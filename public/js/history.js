
var gameSkip = 0;
var gamesPer = 7;
var sent = false;

function showGames(skip) {
  var table = $('#recent-games');
  addGameTable(function(){
    table.css('display', 'none');
    table.fadeIn('slow');
  });
}

function addGameTable(callback)
{
  callback = callback | false;
  sent = true;
  $("#load-games-btn").addClass('disabled');
  var target = document.getElementById('recent-games');
  var table = $('#recent-games');
  table.css('min-height','20px');
  opts.top = table.height()+190+'px';
  spinner.spin(target);
  $.ajax({ url: window.location.href+'games/get/'+gameSkip,
    type: 'GET',
    dataType: 'json'
  })
  .done(function(data) {
    finishLoad();
    table.append(data.table);
    $('.session-game').unbind( "click" );
    $('.session-game').click(function(event){
      viewScoreboard($(event.target));
    });
    gameSkip += gamesPer;
    if(callback)
      callback();
  })
  .fail(function() {
    finishLoad();
    console.log("Something went wrong!");
    if(callback)
      callback();
  });
}

function finishLoad()
{
  sent = false;
  spinner.stop();
  $("#load-games-btn").removeClass('disabled');
}

function viewScoreboard(game)
{
  var id = Number(game.attr('data-id'))
  var open = true;
  if(!$("#gameboard-"+id).hasClass('hidden'))
    var open = false;

  $(".gameboard").addClass('hidden');
  $(".session-game").css('color', '#333');

  if(open) {
    $("#gameboard-"+id).removeClass('hidden');
    game.css('color', '#5cb85c');
  }
}

$( document ).ready(function() {
  var win = $(window);
  // Each time the user scrolls
  win.scroll(function() {
    // End of the document reached?
    if ($(document).height() - win.height() - 120 <= win.scrollTop()) {
      if(!sent)
        addGameTable();
    }
  });
});