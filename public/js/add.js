$( document ).ready(function() {
  highlightFrame(currentFrame, currentBall);
  $('.score-btn').click(function(event){
    recordScore($(event.target), Number($(event.target).attr('value')));
  });

  $('.pin').click(function(event){
    markPin($(event.target));
  });
});