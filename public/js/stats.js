// var spinners = {};
// $( document ).ready(function() {
//   var opts = {lines: 11 // The number of lines to draw
//             , length: 0 // The length of each line
//             , width: 14 // The line thickness
//             , radius: 30 // The radius of the inner circle
//             , scale: 1 // Scales overall size of the spinner
//             , corners: 1 // Corner roundness (0..1)
//             , color: '#363636' // #rgb or #rrggbb or array of colors
//             , opacity: 0 // Opacity of the lines
//             , rotate: 0 // The rotation offset
//             , direction: 1 // 1: clockwise, -1: counterclockwise
//             , speed: 1 // Rounds per second
//             , trail: 90 // Afterglow percentage
//             , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
//             , zIndex: 2e9 // The z-index (defaults to 2000000000)
//             , className: 'spinner' // The CSS class to assign to the spinner
//             , top: '50%' // Top position relative to parent
//             , left: '50%' // Left position relative to parent
//             , shadow: false // Whether to render a shadow
//             , hwaccel: false // Whether to use hardware acceleration
//             , position: 'absolute' // Element positioning
//             }
//   var charts = document.getElementsByClassName('chart');
//   for(var i=0; i<charts.length; i++)
//     spinners[charts[i].id] = new Spinner(opts).spin(charts[i])
// });

function drawFirstBallPie(firstBalls) {
  var firstStats = [['pins', 'percent']];
  for(something in firstBalls)
    firstStats.push([something, Number(firstBalls[something])]);

  var data = google.visualization.arrayToDataTable(firstStats);

  var options = {fontName: 'Montserrat', fontSize: 20,
                 chartArea:{width:'100%',height:'100%'},
                 legend:{position: 'labeled'},
                 slices: {0: {color:'#bd0026'},
                          1: {color:'#e31a1c'},
                          2: {color:'#fc4e2a'},
                          3: {color:'#fd8d3c'},
                          4: {color:'#feb24c', textStyle:{color:'black'}},
                          5: {color:'#ffeda0', textStyle:{color:'black'}},
                          6: {color:'#f7fcb9', textStyle:{color:'black'}},
                          7: {color:'#d9f0a3', textStyle:{color:'black'}},
                          8: {color:'#addd8e'},
                          9: {color:'#78c679'},
                          10: {color:'#41ab5d'}}};

  var chart = new google.visualization.PieChart(document.getElementById('firstBallPie'));

  chart.draw(data, options);
  $("#firstBallPie").fadeIn('slow');
  // spinners.firstBallPie.stop();
}

function drawSecondBallPie(spares) {
  var spareStats = [['type', 'percent']];
  spareStats.push(['Spares', spares]);
  spareStats.push(['Opens', 100-spares]);

  var data = google.visualization.arrayToDataTable(spareStats);

  var options = {fontName: 'Montserrat', fontSize: 35,
                 chartArea:{width:'100%',height:'100%'},
                 legend:{position: 'labeled'},
                 slices: {0: {color:'dodgerblue'},
                          1: {color:'firebrick'}}};

  var chart = new google.visualization.PieChart(document.getElementById('secondBallPie'));

  chart.draw(data, options);
  $("#secondBallPie").fadeIn('slow');
  // spinners.secondBallPie.stop();
}

function drawGamePie(gameBalls) {
  var gameStats = [['type', 'count']];
  for(something in gameBalls)
    gameStats.push([something, Number(gameBalls[something])]);

  var data = google.visualization.arrayToDataTable(gameStats);

  var options = {fontName: 'Montserrat', fontSize: 35,
                 pieSliceText: 'value',
                 chartArea:{width:'100%',height:'100%'},
                 legend:{position: 'labeled'},
                 slices: {0: {color:'limegreen'},
                          1: {color:'dodgerblue'},
                          2: {color:'firebrick'}}};

  var chart = new google.visualization.PieChart(document.getElementById('gamePie'));

  chart.draw(data, options);
  $("#gamePie").fadeIn('slow');
  // spinners.gamePie.stop();
}

function drawSparePies(spareStats) {
  google.load("visualization", "1", {packages:["corechart"]});
  
  for(pins in spareStats) {
    var pie = pins+'sparePie';
    var pinStats = [['pins', 'percent']];

    for(leaves in spareStats[pins])
      pinStats.push([leaves, Number(spareStats[pins][leaves])]);

    var data = google.visualization.arrayToDataTable(pinStats);

    var sliceNum = pinStats.length - 1;
    var sliceColors = {1:{0: {color:'#f7f7f7', textStyle:{color:'black'}}},
                       2:{0: {color:'#f7f7f7', textStyle:{color:'black'}},
                          1: {color:'#000000'}},
                       3:{0: {color:'#f7f7f7', textStyle:{color:'black'}},
                          1: {color:'#969696', textStyle:{color:'black'}},
                          2: {color:'#000000'}},
                       4:{0: {color:'#f7f7f7', textStyle:{color:'black'}},
                          1: {color:'#d9d9d9', textStyle:{color:'black'}},
                          2: {color:'#525252'},
                          3: {color:'#000000'}},
                       5:{0: {color:'#f7f7f7', textStyle:{color:'black'}},
                          1: {color:'#d9d9d9', textStyle:{color:'black'}},
                          2: {color:'#969696', textStyle:{color:'black'}},
                          3: {color:'#525252'},
                          4: {color:'#000000'}},
                       6:{0: {color:'#f7f7f7', textStyle:{color:'black'}},
                          1: {color:'#f0f0f0', textStyle:{color:'black'}},
                          2: {color:'#d9d9d9', textStyle:{color:'black'}},
                          3: {color:'#525252'},
                          4: {color:'#252525'},
                          5: {color:'#000000'}},
                       7:{0: {color:'#f7f7f7', textStyle:{color:'black'}},
                          1: {color:'#f0f0f0', textStyle:{color:'black'}},
                          2: {color:'#d9d9d9', textStyle:{color:'black'}},
                          3: {color:'#969696', textStyle:{color:'black'}},
                          4: {color:'#525252'},
                          5: {color:'#252525'},
                          6: {color:'#000000'}},
                       8:{0: {color:'#f7f7f7', textStyle:{color:'black'}},
                          1: {color:'#f0f0f0', textStyle:{color:'black'}},
                          2: {color:'#d9d9d9', textStyle:{color:'black'}},
                          3: {color:'#bdbdbd', textStyle:{color:'black'}},
                          4: {color:'#737373'},
                          5: {color:'#525252'},
                          6: {color:'#252525'},
                          7: {color:'#000000'}},
                       9:{0: {color:'#f7f7f7', textStyle:{color:'black'}},
                          1: {color:'#f0f0f0', textStyle:{color:'black'}},
                          2: {color:'#d9d9d9', textStyle:{color:'black'}},
                          3: {color:'#bdbdbd', textStyle:{color:'black'}},
                          4: {color:'#969696', textStyle:{color:'black'}},
                          5: {color:'#737373'},
                          6: {color:'#525252'},
                          7: {color:'#252525'},
                          8: {color:'#000000'}},
                       10:{0: {color:'#f7f7f7', textStyle:{color:'black'}},
                          1: {color:'#f0f0f0', textStyle:{color:'black'}},
                          2: {color:'#d9d9d9', textStyle:{color:'black'}},
                          3: {color:'#bdbdbd', textStyle:{color:'black'}},
                          4: {color:'#969696', textStyle:{color:'black'}},
                          5: {color:'#737373'},
                          6: {color:'#636363'},
                          7: {color:'#525252'},
                          8: {color:'#252525'},
                          9: {color:'#000000'}},
                       11:{0: {color:'#f7f7f7', textStyle:{color:'black'}},
                          1: {color:'#f0f0f0', textStyle:{color:'black'}},
                          2: {color:'#d9d9d9', textStyle:{color:'black'}},
                          3: {color:'#cccccc', textStyle:{color:'black'}},
                          4: {color:'#bdbdbd', textStyle:{color:'black'}},
                          5: {color:'#969696', textStyle:{color:'black'}},
                          6: {color:'#737373'},
                          7: {color:'#636363'},
                          8: {color:'#525252'},
                          9: {color:'#252525'},
                          10: {color:'#000000'}}};

    var options = {fontName: 'Montserrat', fontSize: 20,
                   chartArea:{width:'100%',height:'100%'},
                   legend:{position: 'labeled'},
                   slices: sliceColors[sliceNum]};

    var chart = new google.visualization.PieChart(document.getElementById(pie));

    chart.draw(data, options);
    // spinners.pie.stop();
  }
}