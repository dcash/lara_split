function highlightFrame(frame, ball) {
  $('.ball1, .ball2, .ball3').removeClass('current-ball');
  $('#frame'+frame+' .ball'+ball).addClass('current-ball');
}

var totalScore = 0;
var currentFrame = 1;
var currentBall = 1;
var ballOne = 0;
var frameScores = {1:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball1Pins:[],
                      ball2:0,
                      ball2Pins:[],
                      strike:false,
                      spare:false},
                   2:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball1Pins:[],
                      ball2:0,
                      ball2Pins:[],
                      strike:false,
                      spare:false},
                   3:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball1Pins:[],
                      ball2:0,
                      ball2Pins:[],
                      strike:false,
                      spare:false},
                   4:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball1Pins:[],
                      ball2:0,
                      ball2Pins:[],
                      strike:false,
                      spare:false},
                   5:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball1Pins:[],
                      ball2:0,
                      ball2Pins:[],
                      strike:false,
                      spare:false},
                   6:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball1Pins:[],
                      ball2:0,
                      ball2Pins:[],
                      strike:false,
                      spare:false},
                   7:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball1Pins:[],
                      ball2:0,
                      ball2Pins:[],
                      strike:false,
                      spare:false},
                   8:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball1Pins:[],
                      ball2:0,
                      ball2Pins:[],
                      strike:false,
                      spare:false},
                   9:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball1Pins:[],
                      ball2:0,
                      ball2Pins:[],
                      strike:false,
                      spare:false},
                   10:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball1Pins:[],
                      ball2:0,
                      ball2Pins:[],
                      ball3:0,
                      ball3Pins:[],
                      strike:false,
                      spare:false}};

function disableButtons(pins) {
  for (var i = pins; i < 10; i++) {
    $('#score'+i).addClass('disabled');
  }
  if(arguments[1])
    $('#score10').addClass('disabled');
  else {
    $("#score10").attr('value', pins);
    $("#score10").text("spare");
  }
}

function enableButtons() {
  for (var i = 0; i <= 10; i++) {
    $('#score'+i).removeClass('disabled');
  }
  $("#score10").attr('value', 10);
  $("#score10").text("strike");
  $('.pin').removeClass('disabled');
}

function addScore(score) {
  totalScore += score;
}

function removeScore() {
  if($('#saveGame').hasClass('disabled'))
    currentBall--;
  else {
    $('#saveGame').addClass('disabled');
    enableButtons();
  }
  if(currentBall == 0) {
    if(currentFrame == 1) {
      currentBall = 1;
      $("#scoreCLR").addClass('disabled');
    }
    else {
      currentBall = 2;
      currentFrame--;
    }
  }
  if(currentFrame < 10 && frameScores[currentFrame].strike) {
    frameScores[currentFrame].strike = false;
    currentBall = 1;
  }
  if(currentBall == 2) {
    if(currentFrame == 10) {
      if(frameScores[currentFrame]['ball1'] == 10){
        frameScores[10].score -= frameScores[currentFrame]['ball2'];
        ballOne = frameScores[currentFrame]['ball1'];
        enableButtons();
      }
      else {
        if(frameScores[currentFrame]['ball1'] + frameScores[currentFrame]['ball2'] < 10)
          totalScore -= frameScores[currentFrame]['ball2']+frameScores[currentFrame]['ball1'];
        disableButtons(10-frameScores[currentFrame]['ball1']);
      }
      if(frameScores[9].strike) {
        totalScore -= frameScores[9].score+frameScores[9].plus1+frameScores[9].plus2;
        frameScores[9].plus2 = 0;
        $('#frame'+(9)+' .total').html('');
      }
    }
    else {
      disableButtons(10-frameScores[currentFrame]['ball1']);
      if(frameScores[currentFrame].spare)
        frameScores[currentFrame].spare = false;
      else
        totalScore -= frameScores[currentFrame]['ball2']+frameScores[currentFrame]['ball1'];
      if(currentFrame > 1 && frameScores[currentFrame-1].strike) {
        totalScore -= frameScores[currentFrame-1].score+frameScores[currentFrame-1].plus1+frameScores[currentFrame-1].plus2;
        frameScores[currentFrame-1].plus2 = 0;
        $('#frame'+(currentFrame-1)+' .total').html('');
      }
    }
  }
  else if(currentBall == 3){
    if(frameScores[currentFrame]['ball2'] == 10 || frameScores[currentFrame]['ball1'] + frameScores[currentFrame]['ball2'] == 10)
      enableButtons();
    else
      disableButtons(10-frameScores[currentFrame]['ball2']);
    if(frameScores[currentFrame].ball1 + frameScores[currentFrame].ball2 == 10){
      totalScore -= frameScores[currentFrame].score+frameScores[currentFrame].plus2;
      frameScores[currentFrame].plus1 = 0;
    }
    else {
      totalScore -= frameScores[currentFrame].score+frameScores[currentFrame].plus1+frameScores[currentFrame].plus2;
      frameScores[currentFrame].plus2 = 0;
    }
  }
  else {
    enableButtons();
    if(currentFrame > 1 && frameScores[currentFrame-1].spare) {
      totalScore -= frameScores[currentFrame-1].score+frameScores[currentFrame-1].plus1;
      frameScores[currentFrame-1].plus1 = 0;
      $('#frame'+(currentFrame-1)+' .total').html('');
    }
    if(currentFrame > 1 && frameScores[currentFrame-1].strike) {
      frameScores[currentFrame-1].plus1 = 0;
      if(currentFrame > 2 && frameScores[currentFrame-2].strike) {
        totalScore -= frameScores[currentFrame-2].score+frameScores[currentFrame-2].plus1+frameScores[currentFrame-2].plus2;
        frameScores[currentFrame-2].plus2 = 0;
        $('#frame'+(currentFrame-2)+' .total').html('');
      }
    }
  }

  highlightFrame(currentFrame, currentBall);
  $('#frame'+currentFrame+' .ball'+currentBall).html("");
  $('#frame'+currentFrame+' .total').html("");
  frameScores[currentFrame]['ball'+currentBall] = 0;
  pinCount = 0;
  framePins[currentFrame][currentBall] = [];
  saveFramePins();
}

function displayPinfall(frame, ball, pins) {
  if(frame == 10) {
    if(ball == 1) {
      if(pins == 10)
        $('#frame'+frame+' .ball'+ball).html("X");
      else
        $('#frame'+frame+' .ball'+ball).html(pins);
    }
    else if(ball >= 2) {
      if(ballOne < 10 && ballOne+pins == 10)
        $('#frame'+frame+' .ball'+ball).html("/");
      else if(pins == 10)
        $('#frame'+frame+' .ball'+ball).html("X");
      else
        $('#frame'+frame+' .ball'+ball).html(pins);
    }
  }
  else {
    if(ball == 1 && pins == 10)
      $('#frame'+frame+' .ball1').html("X");
    else if(ball == 2 && ballOne+pins == 10)
      $('#frame'+frame+' .ball2').html("/");
    else
      $('#frame'+frame+' .ball'+ball).html(pins);
  }
  frameScores[frame]['ball'+ball] = pins;
}

function displayScore(frame, score) {
  frameScores[frame].totalScore = score;
  $('#frame'+frame+' .total').html(score);
}

var strikePins = [1,2,3,4,5,6,7,8,9,10];

function checkStrike(pins) {
  if(pins == 10) {
    if(currentBall == 1) {
      frameScores[currentFrame].strike = true;
      frameScores[currentFrame].score = 10;
      framePins[currentFrame][currentBall] = strikePins;
    }
    if(currentFrame < 10)
      currentFrame++;
    else {
      ballOne = pins;
      currentBall = 2;
      enableButtons();
    }
  }
  else {
    ballOne = pins;
    disableButtons(10-pins);
    currentBall = 2;
  }
}

function countStrikeSpare(frame, pins) {
  if(frameScores[frame].spare)
    frameScores[frame].plus1 = pins;
  else
    frameScores[frame].plus2 = pins;

  addScore(frameScores[frame].score+frameScores[frame].plus1+frameScores[frame].plus2);
  displayScore(frame, totalScore);
}

function recordScore(sender, pins) {
  var senderId = sender.attr('id');
  if($('#input-pins').hasClass('active') && senderId == 'score10') {
    for(var p=1; p<11; p++) {
      if(pins == 10)
        framePins[currentFrame][currentBall].push(p);
      else {
        var pindex = framePins[currentFrame][currentBall-1].indexOf(p);
        if(pindex < 0)
          framePins[currentFrame][currentBall].push(p);
      }
    }
    $('.pin').addClass('disabled');
    pinCount = pins;
  }
  else {
    lastPinCount = pins;
    displayPinfall(currentFrame, currentBall, pins);

    if(currentFrame == 10) {
      if(currentBall == 1) {
          if(frameScores[9].strike) {
            frameScores[9].plus1 = pins;
            if(frameScores[8].strike)
              countStrikeSpare(8, pins);
            checkStrike(pins);
          }
          else if(frameScores[9].spare) {
            countStrikeSpare(9, pins);
            checkStrike(pins);
          }
          else
            checkStrike(pins);

          highlightFrame(10, 2);
      }
      else if(currentBall == 2) {
        if(frameScores[9].strike) {
          countStrikeSpare(9, pins);
          displayScore(9, totalScore);
        }
        if(ballOne + pins >= 10) {
          frameScores[10].score = ballOne + pins;
          currentBall = 3;
          highlightFrame(10, 3);
          if(pins < 10)
          {
            if(ballOne + pins == 10)
              enableButtons();
            else {
              disableButtons(10-pins);
              ballOne = pins;
            }
          }
          else {
            enableButtons();
            ballOne = pins;
            framePins[currentFrame][2] = strikePins;
          }
        }
        else {
          addScore(ballOne+pins);
          displayScore(10, totalScore);
          disableButtons(0, true);
          $('#saveGame').removeClass('disabled');
        }
      }
      else if(currentBall == 3) {
        countStrikeSpare(10, pins)
        disableButtons(0, true);
        $('#saveGame').removeClass('disabled');
      }
    }
    else {
      if(currentBall == 1) {
        if(currentFrame > 1) {
          if(frameScores[currentFrame-1].strike) {
            frameScores[currentFrame-1].plus1 = pins;
            if(currentFrame > 2) {
              if(frameScores[currentFrame-2].strike)
                countStrikeSpare(currentFrame-2, pins);
            }
            checkStrike(pins);
          }
          else if(frameScores[currentFrame-1].spare) {
            countStrikeSpare(currentFrame-1, pins);
            checkStrike(pins);
          }
          else
            checkStrike(pins);
        }
        else {
          checkStrike(pins);
          $("#scoreCLR").removeClass('disabled');
        }
      }
      else if(currentBall == 2) {
        if(currentFrame > 1) {
          if(frameScores[currentFrame-1].strike)
            countStrikeSpare(currentFrame-1, pins);
        }
        if(ballOne + pins == 10) {
          frameScores[currentFrame].spare = true;
          frameScores[currentFrame].score = 10;
        }
        else {
          addScore(ballOne+pins);
          displayScore(currentFrame, totalScore);
        }
        enableButtons();
        currentBall = 1;
        currentFrame++;
      }
      highlightFrame(currentFrame, currentBall);
    }
  }
}

var framePins = {1:{1:[], 2:[]},
                 2:{1:[], 2:[]},
                 3:{1:[], 2:[]},
                 4:{1:[], 2:[]},
                 5:{1:[], 2:[]},
                 6:{1:[], 2:[]},
                 7:{1:[], 2:[]},
                 8:{1:[], 2:[]},
                 9:{1:[], 2:[]},
                10:{1:[], 2:[], 3:[]},
               };


var pinCount = 0;
var usePins = false;
function markPin(pin)
{
  var pinNo = Number(pin.text());
  if(pin.hasClass('disabled')) {
    pin.removeClass('disabled');
    var pindex = framePins[currentFrame][currentBall].indexOf(pinNo);
    framePins[currentFrame][currentBall].splice(pindex,1);
    saveFramePins();
    pinCount--;
  }
  else {
    pin.addClass('disabled');
    framePins[currentFrame][currentBall].push(pinNo);
    saveFramePins();
    pinCount++;
  }
}

function recordPins()
{
  usePins = true;
  saveFramePins();
  recordScore($('#doneBtn'), pinCount);
  pinCount = 0;
  if(currentBall == 1)
  {
    $('.pin').removeClass('disabled');
  }
}

function saveFramePins()
{
  frameScores[currentFrame]['ball'+currentBall+'Pins'] = framePins[currentFrame][currentBall];
}

var opts = {lines: 11 // The number of lines to draw
            , length: 0 // The length of each line
            , width: 14 // The line thickness
            , radius: 30 // The radius of the inner circle
            , scale: 1 // Scales overall size of the spinner
            , corners: 1 // Corner roundness (0..1)
            , color: '#363636' // #rgb or #rrggbb or array of colors
            , opacity: 0 // Opacity of the lines
            , rotate: 0 // The rotation offset
            , direction: 1 // 1: clockwise, -1: counterclockwise
            , speed: 1 // Rounds per second
            , trail: 90 // Afterglow percentage
            , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
            , zIndex: 2e9 // The z-index (defaults to 2000000000)
            , className: 'spinner' // The CSS class to assign to the spinner
            , top: '40px' // Top position relative to parent
            , left: '50%' // Left position relative to parent
            , shadow: false // Whether to render a shadow
            , hwaccel: false // Whether to use hardware acceleration
            , position: 'absolute' // Element positioning
            }
var spinner = new Spinner(opts);

function saveGame() {
  opts.top = "40px";
  var tmz = new Date().getTimezoneOffset();
  
  $('#saveWait').removeClass('hidden');
  var target = document.getElementById('saveWait');
  spinner.spin(target);
  var pinfalls = getPinfall();
  var scoreboard = {1:{1:$('#frame1 .ball1').html(),
                       2:$('#frame1 .ball2').html(),
                       '1pins':pinfalls[1][1],
                       '2pins':pinfalls[1][2],
                       score:$('#frame1 .total').html()},
                    2:{1:$('#frame2 .ball1').html(),
                       2:$('#frame2 .ball2').html(),
                       '1pins':pinfalls[2][1],
                       '2pins':pinfalls[2][2],
                       score:$('#frame2 .total').html()},
                    3:{1:$('#frame3 .ball1').html(),
                       2:$('#frame3 .ball2').html(),
                       '1pins':pinfalls[3][1],
                       '2pins':pinfalls[3][2],
                       score:$('#frame3 .total').html()},
                    4:{1:$('#frame4 .ball1').html(),
                       2:$('#frame4 .ball2').html(),
                       '1pins':pinfalls[4][1],
                       '2pins':pinfalls[4][2],
                       score:$('#frame4 .total').html()},
                    5:{1:$('#frame5 .ball1').html(),
                       2:$('#frame5 .ball2').html(),
                       '1pins':pinfalls[5][1],
                       '2pins':pinfalls[5][2],
                       score:$('#frame5 .total').html()},
                    6:{1:$('#frame6 .ball1').html(),
                       2:$('#frame6 .ball2').html(),
                       '1pins':pinfalls[6][1],
                       '2pins':pinfalls[6][2],
                       score:$('#frame6 .total').html()},
                    7:{1:$('#frame7 .ball1').html(),
                       2:$('#frame7 .ball2').html(),
                       '1pins':pinfalls[7][1],
                       '2pins':pinfalls[7][2],
                       score:$('#frame7 .total').html()},
                    8:{1:$('#frame8 .ball1').html(),
                       2:$('#frame8 .ball2').html(),
                       '1pins':pinfalls[8][1],
                       '2pins':pinfalls[8][2],
                       score:$('#frame8 .total').html()},
                    9:{1:$('#frame9 .ball1').html(),
                       2:$('#frame9 .ball2').html(),
                       '1pins':pinfalls[9][1],
                       '2pins':pinfalls[9][2],
                       score:$('#frame9 .total').html()},
                    10:{1:$('#frame10 .ball1').html(),
                       2:$('#frame10 .ball2').html(),
                       3:$('#frame10 .ball3').html(),
                       '1pins':pinfalls[10][1],
                       '2pins':pinfalls[10][2],
                       '3pins':pinfalls[10][3],
                       score:$('#frame10 .total').html()},
                    total:totalScore,
                    timezone:tmz};

  $.ajax({ url: window.location.href.replace('create','save'),
      type: 'POST',
      data: scoreboard
    })
    .done(function(result) {
      $('#saveWait').addClass('hidden');
      spinner.stop();
      if(result.result == "success") {
        $('#saveFail').addClass('hidden');
        $('#saveSuccess').removeClass('hidden');
        $('#newGame').removeClass('hidden');
        $('#saveGame').addClass('hidden');
      }
      else {
        $('#saveFail').removeClass('hidden');
        $('#saveGame').text('Try Again');
      }
    })
    .fail(function() {
      $('#saveWait').addClass('hidden');
      spinner.stop();
      $('#saveFail').removeClass('hidden');
      $('#saveGame').text('Try Again');
    });
}

function getPinfall()
{
  var pinfalls = {1:{1:null, 2:null},
                  2:{1:null, 2:null},
                  3:{1:null, 2:null},
                  4:{1:null, 2:null},
                  5:{1:null, 2:null},
                  6:{1:null, 2:null},
                  7:{1:null, 2:null},
                  8:{1:null, 2:null},
                  9:{1:null, 2:null},
                  10:{1:null, 2:null, 3:null}};

  if(usePins) {
    for(var i=1; i<11; i++) {
      var firstPins = framePins[i][1];
      var secondPins = framePins[i][2];
      pinfalls[i][1] = "";
      pinfalls[i][2] = "";
      if(i == 10) {
        var thirdPins = framePins[i][3];
        pinfalls[i][3] = "";
      }
      var balls = (i == 10) ? 3:2;
      for(var j=1; j<=balls; j++) {
        for(var p=1; p<11; p++) {
          if(framePins[i][j].indexOf(p) >= 0) {
            pinfalls[i][j] += "1";
          }
          else {
            if(j >1 && framePins[i][j-1].indexOf(p) >= 0 && $('#frame'+i+' .ball'+(j-1)).html() != 'X') {
              pinfalls[i][j] += "-";
            }
            else {
              pinfalls[i][j] += "0";
            }
          }
        }
      }
    }
  }

  return pinfalls;
}
