<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePinfallTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinfalls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pinfall', 10);
            $table->timestamps();
        });

        Schema::table('frames', function ($table) {
            $table->integer('ball1_pins')->unsigned()->nullable();
            $table->integer('ball2_pins')->unsigned()->nullable();
            $table->integer('ball3_pins')->unsigned()->nullable();
            $table->foreign('ball1_pins')->references('id')->on('pinfalls');
            $table->foreign('ball2_pins')->references('id')->on('pinfalls');
            $table->foreign('ball3_pins')->references('id')->on('pinfalls');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('frames', function ($table) {
            $table->dropForeign(['ball1_pins']);
            $table->dropForeign(['ball2_pins']);
            $table->dropForeign(['ball3_pins']);
            $table->dropColumn('ball1_pins');
            $table->dropColumn('ball2_pins');
            $table->dropColumn('ball3_pins');
        });
        Schema::drop('pinfalls');
    }
}
