<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frames', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ball_1', 2);
            $table->string('ball_2', 2);
            $table->string('ball_3', 2)->nullable();
            $table->integer('score')->unsigned();
            $table->timestamps();
        });
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('frame_1')->unsigned();
            $table->integer('frame_2')->unsigned();
            $table->integer('frame_3')->unsigned();
            $table->integer('frame_4')->unsigned();
            $table->integer('frame_5')->unsigned();
            $table->integer('frame_6')->unsigned();
            $table->integer('frame_7')->unsigned();
            $table->integer('frame_8')->unsigned();
            $table->integer('frame_9')->unsigned();
            $table->integer('frame_10')->unsigned();
            $table->integer('score')->unsigned();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('frame_1')->references('id')->on('frames');
            $table->foreign('frame_2')->references('id')->on('frames');
            $table->foreign('frame_3')->references('id')->on('frames');
            $table->foreign('frame_4')->references('id')->on('frames');
            $table->foreign('frame_5')->references('id')->on('frames');
            $table->foreign('frame_6')->references('id')->on('frames');
            $table->foreign('frame_7')->references('id')->on('frames');
            $table->foreign('frame_8')->references('id')->on('frames');
            $table->foreign('frame_9')->references('id')->on('frames');
            $table->foreign('frame_10')->references('id')->on('frames');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('games');
        Schema::drop('frames');
    }
}
