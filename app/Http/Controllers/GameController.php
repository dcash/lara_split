<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Repositories\GameRepository;

use App\Http\Requests;
use App\Game;
use App\Frame;
use App\Pinfall;

class GameController extends Controller
{
  protected $games;
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(GameRepository $games)
  {
    $this->middleware('auth');
    $this->games = $games;
  }

  public function create() {
    return view('game/create');
  }

  public function retrieve(Request $request, $skip)
  {
    $this->games->user = $request->user();
    $this->games->skip = $skip;
    $games = $this->games->gameArrayByDate();
    // print_r($games);
    $gameTable = view('home.gameTableCollection', ['games'=>$games])->render();
    return response()->json(['table'=>$gameTable]);
  }

  public function save(Request $request)
  {
    $scoreboard = $request->all();

    $game = new Game;

    $game->user_id = $request->user()->id;

    for ($i=1; $i<11; $i++) {
      $frameData = $scoreboard[$i];

      

      $frame = new Frame;
      $frame->ball_1 = $frameData[1];
      $frame->ball_2 = $frameData[2];
      $frame->ball_3 = (isset($frameData[3])) ? $frameData[3]:null;
      $frame->ball1_pins = Pinfall::firstOrCreate(['pinfall' => $frameData['1pins']])->id;
      $frame->ball2_pins = Pinfall::firstOrCreate(['pinfall' => $frameData['2pins']])->id;
      $frame->ball3_pins = (isset($frameData['3pins'])) ? Pinfall::firstOrCreate(['pinfall' => $frameData['3pins']])->id:null;
      $frame->score = $frameData['score'];

      $frame->save();

      $game->{"frame_$i"} = $frame->id;
    }

    $game->score = $scoreboard['total'];

    $game->save();

    return response()->json(['result'=>'success']);
  }
}
