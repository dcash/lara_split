<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Repositories\GameRepository;

use App\Http\Requests;
use App\Game;
use App\Frame;

class StatsController extends Controller
{
  protected $games;

  public function __construct(GameRepository $games)
  {
    $this->middleware('auth');
    $this->games = $games;
  }

  public function index(Request $request)
  {
    $this->games->user = $request->user();
    $this->games->limit = 1000000;
    $this->games->skip = 0;
    $games = $this->games->userGameArray();
    return view('stats.stats', [
      'stats' => $this->statsForGames($games)
    ]);
  }
  public function day(Request $request, $day)
  {
    $this->games->user = $request->user();
    $games = $this->games->dateGameArray($day);
    return view('stats.stats', [
      'stats' => $this->statsForGames($games),
      'day' => $day
    ]);
  }
  public function game(Request $request, $game, $number)
  {
    $this->games->user = $request->user();
    $games = $this->games->gameArray($game);
    return view('stats.stats', [
      'stats' => $this->statsForGames([$games]),
      'day' => $games['created_at'],
      'number' => $number
    ]);
  }

  private function statsForGames($games)
  {
    $totalScore = 0;
    $frameStats = [1=>['strikes'=>0,'spares'=>0,'opens'=>0,'score'=>0,'balls'=>0,'avg'=>0],
                    2=>['strikes'=>0,'spares'=>0,'opens'=>0,'score'=>0,'balls'=>0,'avg'=>0],
                    3=>['strikes'=>0,'spares'=>0,'opens'=>0,'score'=>0,'balls'=>0,'avg'=>0],
                    4=>['strikes'=>0,'spares'=>0,'opens'=>0,'score'=>0,'balls'=>0,'avg'=>0],
                    5=>['strikes'=>0,'spares'=>0,'opens'=>0,'score'=>0,'balls'=>0,'avg'=>0],
                    6=>['strikes'=>0,'spares'=>0,'opens'=>0,'score'=>0,'balls'=>0,'avg'=>0],
                    7=>['strikes'=>0,'spares'=>0,'opens'=>0,'score'=>0,'balls'=>0,'avg'=>0],
                    8=>['strikes'=>0,'spares'=>0,'opens'=>0,'score'=>0,'balls'=>0,'avg'=>0],
                    9=>['strikes'=>0,'spares'=>0,'opens'=>0,'score'=>0,'balls'=>0,'avg'=>0],
                    10=>['strikes'=>0,'spares'=>0,'opens'=>0,'score'=>0,'balls'=>0,'avg'=>0]];
    $spareStats = [];
    $firstPins = [0=>0,
                   1=>0,
                   2=>0,
                   3=>0,
                   4=>0,
                   5=>0,
                   6=>0,
                   7=>0,
                   8=>0,
                   9=>0,
                   'X'=>0];

    $counts = ['games'=>0,
               'balls'=>0,
               'firstBalls'=>0,
               'secondBalls'=>0,
               'secBall'=>0,
               'avgOpen'=>0,
               'strikes'=>0,
               'spares'=>0,
               'mostStrikes'=>0,
               'streak'=>0,
               'highScore'=>0];

    foreach ($games as $game) {
      $gameStreak = 0;
      $highStreak = 0;
      $gameStrikes = 0;

      for($i=1; $i<10; $i++) {
        $ballOne = $game["frame_$i"]['ball_1'];
        $counts['firstBalls']++;
        $counts['balls']++;
        $frameStats[$i]['balls']++;
        if($ballOne == 'X') {
          $firstPins['X']++;
          $counts['strikes']++;
          $frameStats[$i]['strikes']++;
          $gameStrikes++;
          $gameStreak++;
        }
        else {
          if($gameStreak > $highStreak)
            $highStreak = $gameStreak;
          $gameStreak = 0;
          $counts['balls']++;
          $frameStats[$i]['balls']++;
          $counts['secondBalls']++;
          $firstPins[$ballOne]++;
          $ballTwo = $game["frame_$i"]['ball_2'];

          $spareStats = $this->addSpareStat($ballOne, $ballTwo, $spareStats);

          if($ballTwo == "/") {
            $counts['spares']++;
            $frameStats[$i]['spares']++;
          }
          else 
            $frameStats[$i]['opens']++;
        }
        $frameStats[$i]['score'] += $game["frame_$i"]['score'];
      }

      $ballOne = $game["frame_10"]['ball_1'];
      $counts['firstBalls']++;
      $counts['balls']++;
      $frameStats[10]['balls']++;
      if($ballOne == 'X') {
        $firstPins['X']++;
        $counts['strikes']++;
        $frameStats[10]['strikes']++;
        $gameStrikes++;
        $gameStreak++;
        $counts['firstBalls']++;
        $counts['balls']++;
        $frameStats[10]['balls']++;
        $ballTwo = $game["frame_10"]['ball_2'];
        if($ballTwo == "X") {
          $firstPins['X']++;
          $counts['strikes']++;
          $frameStats[10]['strikes']++;
          $gameStrikes++;
          $gameStreak++;
          $counts['balls']++;
          $frameStats[10]['balls']++;
          $counts['firstBalls']++;
          $ballThree = $game["frame_10"]['ball_3'];
          if($ballThree == 'X') {
            $firstPins['X']++;
            $counts['strikes']++;
            $frameStats[10]['strikes']++;
            $gameStrikes++;
            $gameStreak++;
          }
          else {
            $firstPins[$ballThree]++;
          }
        }
        else {
          if($gameStreak > $highStreak)
            $highStreak = $gameStreak;
          $gameStreak = 0;
          $firstPins[$ballTwo]++;
          $counts['balls']++;
          $frameStats[10]['balls']++;
          $counts['secondBalls']++;
          $ballThree = $game["frame_10"]['ball_3'];

          $spareStats = $this->addSpareStat($ballTwo, $ballThree, $spareStats);

          if($ballThree == '/') {
            $counts['spares']++;
            $frameStats[10]['spares']++;
          }
          else
            $frameStats[10]['opens']++;
        }
      }
      else {
        if($gameStreak > $highStreak)
          $highStreak = $gameStreak;
        $gameStreak = 0;
        $counts['balls']++;
        $frameStats[10]['balls']++;
        $counts['secondBalls']++;
        $firstPins[$ballOne]++;
        $ballTwo = $game["frame_10"]['ball_2'];

        $spareStats = $this->addSpareStat($ballOne, $ballTwo, $spareStats);

        if($ballTwo == "/") {
          $counts['spares']++;
          $frameStats[10]['spares']++;
          $counts['balls']++;
          $frameStats[10]['balls']++;
          $counts['firstBalls']++;
          $ballThree = $game["frame_10"]['ball_3'];
          if($ballThree == 'X') {
            $firstPins['X']++;
            $counts['strikes']++;
            $frameStats[10]['strikes']++;
            $gameStrikes++;
            $gameStreak++;
          }
          else {
            $firstPins[$ballThree]++;
            $frameStats[10]['opens']++;
          }
        }
        else
          $frameStats[10]['opens']++;
      }

      $frameStats[$i]['score'] += $game["frame_10"]['score'];

      if($gameStrikes > $counts['mostStrikes'])
        $counts['mostStrikes'] = $gameStrikes;

      if($gameStreak > $highStreak)
        $highStreak = $gameStreak;

      if($highStreak > $counts['streak'])
        $counts['streak'] = $highStreak;

      $gameScore = $game['score'];

      if($gameScore > $counts['highScore'])
        $counts['highScore'] = $gameScore;

      $counts['games']++;
      $totalScore += $gameScore;
    }

    $ballOneCounts = [1=>['hits'=>0, 'misses'=>0],
                        2=>['hits'=>0, 'misses'=>0],
                        3=>['hits'=>0, 'misses'=>0],
                        4=>['hits'=>0, 'misses'=>0],
                        5=>['hits'=>0, 'misses'=>0],
                        6=>['hits'=>0, 'misses'=>0],
                        7=>['hits'=>0, 'misses'=>0],
                        8=>['hits'=>0, 'misses'=>0],
                        9=>['hits'=>0, 'misses'=>0],
                        10=>['hits'=>0, 'misses'=>0]];

    $ballTwoCounts = [1=>['hits'=>0, 'misses'=>0],
                        2=>['hits'=>0, 'misses'=>0],
                        3=>['hits'=>0, 'misses'=>0],
                        4=>['hits'=>0, 'misses'=>0],
                        5=>['hits'=>0, 'misses'=>0],
                        6=>['hits'=>0, 'misses'=>0],
                        7=>['hits'=>0, 'misses'=>0],
                        8=>['hits'=>0, 'misses'=>0],
                        9=>['hits'=>0, 'misses'=>0],
                        10=>['hits'=>0, 'misses'=>0]];

    foreach ($games as $game) {
      for($i=1; $i<10; $i++) {
        $ballOnePins = $game["frame_$i"]['ball1_pins'];
        if($ballOnePins) {
          $ballOnePinArray = str_split($ballOnePins['pinfall']);
          $sparePinCount = 0;
          foreach ($ballOnePinArray as $key => $pin) {
            if($pin == 1) {
              $ballOneCounts[$key+1]['hits']++;
            }
            else if($pin == 0) {
              $ballOneCounts[$key+1]['misses']++;
              $sparePinCount++;
            }
          }
          if($sparePinCount == 1) {
            $ballTwoPins = $game["frame_$i"]['ball2_pins'];
            $ballTwoPinArray = str_split($ballTwoPins['pinfall']);
            foreach ($ballTwoPinArray as $key => $pin) {
              if($pin == 1) {
                $ballTwoCounts[$key+1]['hits']++;
              }
              else if($pin == 0) {
                $ballTwoCounts[$key+1]['misses']++;
              }
            }
          }
        }
      }
    }

    $ballOnePercents = [];
    $ballTwoPercents = [];

    foreach ($ballOneCounts as $pin => $countArray) {
      $total = intval($countArray['hits']) + intval($countArray['misses']);
      if($total > 0) {
        $ballOnePercents[$pin] = round(($countArray['hits']/$total)*100, 0, PHP_ROUND_HALF_DOWN);
      }
    }
    foreach ($ballTwoCounts as $pin => $countArray) {
      $total = intval($countArray['hits']) + intval($countArray['misses']);
      if($countArray['hits'] > 0 && $countArray['misses'] > 0) {
        $ballTwoPercents[$pin] = round(($countArray['hits']/$total)*100, 0, PHP_ROUND_HALF_DOWN);
      }
    }

    $avgStrike = 0;
    $avgSpare = 0;
    $avg = round(($totalScore/count($games)), 0, PHP_ROUND_HALF_DOWN);
    if($counts['strikes'] > 0)
      $avgStrike = round(($counts['strikes']/$counts['firstBalls'])*100, 0, PHP_ROUND_HALF_DOWN);
    if($counts['spares'] > 0)
      $avgSpare = round(($counts['spares']/$counts['secondBalls'])*100, 0, PHP_ROUND_HALF_DOWN);

    $seconds = $counts['firstBalls'] - $counts['strikes'];
    $opens = $counts['secondBalls'] - $counts['spares'];

    $avgFirst = [];
    foreach($firstPins as $key=>$pins) {
      $freq = $firstPins[$key];
      $avgFirst[$key] = round(($freq/$counts['firstBalls'])*100, 0, PHP_ROUND_HALF_DOWN);
    }

    $counts['average'] = $avg;
    $counts['avgFirst'] = $avgFirst;
    $counts['avgSpare'] = $avgSpare;
    $counts['avgStrike'] = $avgStrike;

    if($opens > 0)
      $counts['avgOpen'] = round(($opens/$counts['secondBalls'])*100, 0, PHP_ROUND_HALF_DOWN);
    if($seconds > 0)
      $counts['secBall'] = round(($seconds/$counts['firstBalls'])*100, 0, PHP_ROUND_HALF_DOWN);

    $counts['strikesGame'] = round($counts['strikes']/$counts['games'], 0, PHP_ROUND_HALF_DOWN);
    $counts['sparesGame'] = round($counts['spares']/$counts['games'], 0, PHP_ROUND_HALF_DOWN);
    $counts['opensGame'] = round($opens/$counts['games'], 0, PHP_ROUND_HALF_DOWN);

    foreach($frameStats as $key=>$stat) {
      $frameStats[$key]['avg'] = round($frameStats[$key]['score']/$counts['games'], 0, PHP_ROUND_HALF_DOWN);
    }

    $counts['frameStats'] = $frameStats;
    $counts['spareStats'] = $spareStats;

    $counts['ballOnePercents']  = $ballOnePercents;
    $counts['ballTwoPercents']  = $ballTwoPercents;

    return $counts;
  }

  private function addSpareStat($one, $two, $spareStats) {
    $spare = 10-$one;

    if($two == '/')
      $left = 'Spare';
    else if($two == 0)
      $left = 'Miss';
    else
      $left = 'Leave '.($spare-$two);

    if(isset($spareStats[$spare])) {
      if(isset($spareStats[$spare][$left]))
        $spareStats[$spare][$left]++;
      else 
        $spareStats[$spare][$left] = 1;
    }
    else {
      $spareStats[$spare] = [];
      $spareStats[$spare][$left] = 1;
    }
    return $spareStats;
  }
}
