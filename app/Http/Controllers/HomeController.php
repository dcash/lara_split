<?php

namespace App\Http\Controllers;

use App\Game;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\GameRepository;

class HomeController extends Controller
{
  protected $games;
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(GameRepository $games)
  {
    $this->middleware('auth');
    $this->games = $games;
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    return view('home/home');
    return view('home', [
      'games' => $this->games->forUser($request->user()),
    ]);
  }
}
