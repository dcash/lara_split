<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::get('/home', 'HomeController@index');

// Route::get('/games', 'TaskController@index');
// Route::post('/game', 'TaskController@store');
// Route::delete('/game/{game}', 'TaskController@destroy');

// Dashboard
Route::get('/', 'HomeController@index');

// Stats
Route::get('/stats', 'StatsController@index');
Route::get('/stats/day/{day}', 'StatsController@day');
Route::get('/stats/game/{game}/{number}', 'StatsController@game');

// Add Game
Route::get('/games/create', 'GameController@create');
Route::post('/games/save', 'GameController@save');

// Get Games
Route::get('/games/get/{skip}', 'GameController@retrieve');


// app.get('/', account.index);
// app.post('/', account.login);
// app.get('/create-account', account.create);
// app.post('/create-account', account.save);
// app.get("/logout", account.logout);
// app.get("/stats", account.stats);
// app.get("/stats/:day", account.stats);
// app.get("/add-game", requireLogin(), game.index);
// app.post("/save-game", requireLogin(), game.save);

// app.get('/retrieve-games/:skip', account.retrieve_games);

