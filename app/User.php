<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function games()
    {
        return $this->hasMany(Game::class);
    }

    public function playDates($limit, $skip)
    {
        return Game::distinct()->selectRaw('DATE(created_at) as day')
                               ->where('user_id', '=', $this->id)
                               ->orderBy('created_at', 'desc')
                               ->skip($skip)
                               ->take($limit)
                               ->get();
    }
}
