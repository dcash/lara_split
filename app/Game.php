<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Frame;
use App\Pinfall;

class Game extends Model
{
  protected $fillable = [ 'frame_1',
                          'fame_2',
                          'fame_3',
                          'fame_4',
                          'fame_5',
                          'fame_6',
                          'fame_7',
                          'fame_8',
                          'fame_9',
                          'fame_10',
                          'score' ];

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function frame($id)
  {
    $frame = Frame::find($id);
    $frame->ball1_pins = $frame->pinfall($frame->ball1_pins);
    $frame->ball2_pins = $frame->pinfall($frame->ball2_pins);
    $frame->ball3_pins = $frame->pinfall($frame->ball3_pins);
    return $frame;
  }
}
