<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pinfall;

class Frame extends Model
{
  protected $fillable = [ 'ball_1',
                          'ball_2',
                          'ball_3',
                          'score',
                          'ball1_pins',
                          'ball2_pins',
                          'ball3_pins' ];

  public function pinfall($id) {
    return Pinfall::find($id);
  }
}
