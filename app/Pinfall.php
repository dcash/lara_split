<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pinfall extends Model
{
    protected $fillable = [ 'pinfall' ];
    
    public function pinfallID($pinfall) {
      return Pinfall::firstOrCreate(['pinfall' => $pinfall])->id;
    }
}
