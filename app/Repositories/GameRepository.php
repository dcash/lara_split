<?php

namespace App\Repositories;

use App\User;
use App\Game;
use DB;

class GameRepository
{
  public $user;
  public $limit;
  public $skip;

  public function __construct()
  {
    $this->limit = 7;
  }

  /**
   * Get all of the games for a given user.
   *
   * @param  User  $user
   * @return Collection
   */
  public function forUser()
  {
    return $this->user->games()
                      ->orderBy('games.created_at', 'desc')
                      ->skip($this->skip)
                      ->take($this->limit)
                      ->get();
  }

  public function findByDate($date)
  {
    return Game::whereRaw('DATE(created_at) = ?', [$date])->where('user_id', '=', $this->user->id)->get();
  }

  public function userGameArray()
  {
    $return = [];
    $games = $this->forUser();
    foreach ($games as $game) {
      $arrayGame = $game->toArray();
      for($i=1; $i<11; $i++) {
        $arrayGame["frame_$i"] = $game->frame($game->{"frame_$i"})->toArray();
        if($arrayGame["frame_$i"]['ball1_pins'])
          $arrayGame["frame_$i"]['ball1_pins'] = $arrayGame["frame_$i"]['ball1_pins']->toArray();
        if($arrayGame["frame_$i"]['ball2_pins'])
          $arrayGame["frame_$i"]['ball2_pins'] = $arrayGame["frame_$i"]['ball2_pins']->toArray();
        if($arrayGame["frame_$i"]['ball3_pins'])
          $arrayGame["frame_$i"]['ball3_pins'] = $arrayGame["frame_$i"]['ball3_pins']->toArray();
      }
      $return[] = $arrayGame;
    }
    return $return;
  }

  public function dateGameArray($day)
  {
    $return = [];
    $games = $this->findByDate($day);
    foreach ($games as $game) {
      $arrayGame = $game->toArray();
      for($i=1; $i<11; $i++) {
        $arrayGame["frame_$i"] = $game->frame($game->{"frame_$i"})->toArray();
        if($arrayGame["frame_$i"]['ball1_pins'])
          $arrayGame["frame_$i"]['ball1_pins'] = $arrayGame["frame_$i"]['ball1_pins']->toArray();
        if($arrayGame["frame_$i"]['ball2_pins'])
          $arrayGame["frame_$i"]['ball2_pins'] = $arrayGame["frame_$i"]['ball2_pins']->toArray();
        if($arrayGame["frame_$i"]['ball3_pins'])
          $arrayGame["frame_$i"]['ball3_pins'] = $arrayGame["frame_$i"]['ball3_pins']->toArray();
      }
      $return[] = $arrayGame;
    }
    return $return;
  }

  public function gameArray($game)
  {
    $return = [];
    $game = Game::find($game);
    $arrayGame = $game->toArray();
    for($i=1; $i<11; $i++) {
      $arrayGame["frame_$i"] = $game->frame($game->{"frame_$i"})->toArray();
        if($arrayGame["frame_$i"]['ball1_pins'])
          $arrayGame["frame_$i"]['ball1_pins'] = $arrayGame["frame_$i"]['ball1_pins']->toArray();
        if($arrayGame["frame_$i"]['ball2_pins'])
          $arrayGame["frame_$i"]['ball2_pins'] = $arrayGame["frame_$i"]['ball2_pins']->toArray();
        if($arrayGame["frame_$i"]['ball3_pins'])
          $arrayGame["frame_$i"]['ball3_pins'] = $arrayGame["frame_$i"]['ball3_pins']->toArray();
    }
    $return = $arrayGame;
    
    return $return;
  }

  public function gameArrayByDate()
  {
    $return = [];
    $dates = $this->user->playDates($this->limit, $this->skip);
    foreach ($dates as $date) {
      $dateStr = $date->day;
      $games = $this->findByDate($dateStr);
      $returnItem = [];
      $returnItem['date'] = $dateStr;
      $returnItem['games'] = [];
      $average = 0;
      foreach ($games as $game) {
        $arrayGame = $game->toArray();
        for($i=1; $i<11; $i++) {
          $arrayGame["frame_$i"] = $game->frame($game->{"frame_$i"})->toArray();
          if($arrayGame["frame_$i"]['ball1_pins'])
            $arrayGame["frame_$i"]['ball1_pins'] = $arrayGame["frame_$i"]['ball1_pins']->toArray();
          if($arrayGame["frame_$i"]['ball2_pins'])
            $arrayGame["frame_$i"]['ball2_pins'] = $arrayGame["frame_$i"]['ball2_pins']->toArray();
          if($arrayGame["frame_$i"]['ball3_pins'])
            $arrayGame["frame_$i"]['ball3_pins'] = $arrayGame["frame_$i"]['ball3_pins']->toArray();
        }
        $returnItem['games'][] = $arrayGame;
        $average += $game['score'];
      }
      $returnItem['average'] = round($average/count($games), 0, PHP_ROUND_HALF_DOWN);
      $return[] = $returnItem;
    }
    
    return $return;
  }

}

?>