<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Split</title>
    {{-- <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script> --}}
    {{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"/> --}}
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900">
    <script type="text/javascript" src="{{ asset('js/jquery-1.10.2.js') }}"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="{{ asset('css/split.css') }}" rel="stylesheet"/>
    <script type="text/javascript" src="{{ asset('js/spin.min.js') }}"></script>
    <script type="text/javascript">$.ajaxSetup({headers:{'X-CSRF-TOKEN':'{{ csrf_token() }}'}});</script>
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 splitnav">
          <div class="row"><a href="{{ url('/') }}">
              <div class="col-xs-6">
                <div class="dashboard"> 
                  <div class="bowling-ball left"> 
                    <div class="ball-holes">&because;</div>
                  </div>Split
                </div>
              </div>
              </a>
              {{-- <div class="col-xs-6">
                <div class="dashboard"><span class="split-box">Sp<span class="header-frame">it</span></span></div>
              </div> --}}
            <div class="col-xs-6 text-right account-nav">
              @if (Auth::user())
                <div class="btn-group">
                  <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default btn-lg btn-icon btn-ham-icon mobile dropdown-toggle"><span aria-hidden="true" class="glyphicon glyphicon-menu-hamburger"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right mobile">
                    <li class="text-center">Hi {{ Auth::user()->name }}</li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ url('games/create') }}"><span aria-hidden="true" class="glyphicon glyphicon-plus icon-sm"></span> Add Game</a></li>
                    <li><a href="{{ url('stats') }}"><span aria-hidden="true" class="glyphicon glyphicon-signal icon-sm"></span> Stats</a></li>
                    <li><a href="/logout"><span aria-hidden="true" class="glyphicon glyphicon-log-out icon-sm"></span> Logout</a></li>
                  </ul>
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">@yield('content')</div>
      </div>
      @include('layouts.footer')
    </div>
  </body>
</html>