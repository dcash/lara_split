<div id="gameboard-{{ $game['id'] }}" class="row hidden gameboard">
  <a href="{{ url('stats/game/'.$game['id'].'/'.$i) }}">
  @include('game.scoreboard', ['$game'=>$game])
  </a>
</div>