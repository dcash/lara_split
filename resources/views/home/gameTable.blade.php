<div class="row">
  <div class="col-xs-10 col-xs-offset-1">
    <div class="row">
      <a href="{{ url('stats/day/'.$dayGames['date']) }}"><span class="date-num">{{ @formattedDate($dayGames['date']) }} | {{ $dayGames['average'] }}</span></a>
    </div>

    <div class="row">
    <?php $i=1; ?>
    @foreach ($dayGames['games'] as $game)
      @include('home.gameTableItem', ['game'=>$game, 'i'=>$i])
      <?php $i++; ?>
    @endforeach
    </div>

    <?php $i=1; ?>
    @foreach ($dayGames['games'] as $game)
      @include('home.gameboard', ['game'=>$game, 'i'=>$i])
      <?php $i++; ?>
    @endforeach
  </div>
</div>