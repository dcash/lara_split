
@extends('layouts.header')
@section('content')
<div class="row">
  <div class="col-xs-8">
    <h1 class="large">recent sessions</h1>
  </div>
  <div class="col-xs-4 text-right">
    <div class="btn-group">
      <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-info btn-lg btn-icon btn-plus-icon mobile dropdown-toggle"><span aria-hidden="true" class="glyphicon glyphicon-plus"></span></button>
      <ul class="dropdown-menu dropdown-menu-right mobile">
        <li><a href="{{ url('games/create') }}">Add Game</a></li>
      </ul>
    </div>
  </div>
</div>
<div class="row">
  <div id="recent-games"></div>
</div>
<div class="row">
  <div class="col-xs-12 text-center">
    <div id="load-games-btn" class="btn btn-success btn-lg btn-mwide mobile" onclick="showGames(gameSkip)">show games</div>
  </div>
</div>
<script type="text/javascript" src="{{ asset('js/history.js') }}"></script>
<script type="text/javascript">showGames(0);</script>
@endsection