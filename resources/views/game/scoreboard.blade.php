<?php $game = (isset($game)) ? $game:null ?>
<div class="row text-center">
  @for ($i = 1; $i <= 10; $i++)
    @include('game.frame', ['frameNo'=>$i, 'game'=>$game])
  @endfor
</div>