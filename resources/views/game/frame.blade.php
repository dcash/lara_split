<div id="frame{{ $frameNo }}" class="col-xs-1 col-xs-{{ ($frameNo < 10) ? 'ten':'eleven'}} frame">
  <div class="row">
    <div class="col-xs-12 head">{{ $frameNo }}</div>
  </div>
  <div class="row">
    @if ($frameNo < 10)
      <div class="col-xs-6 ball1">{{ $game["frame_$frameNo"]['ball_1'] or '' }}</div>
      <div class="col-xs-6 ball2">{{ $game["frame_$frameNo"]['ball_2'] or '' }}</div>
    @else
      <div class="col-xs-4 ball1">{{ $game["frame_$frameNo"]['ball_1'] or '' }}</div>
      <div class="col-xs-4 ball2">{{ $game["frame_$frameNo"]['ball_2'] or '' }}</div>
      <div class="col-xs-4 ball3">{{ $game["frame_$frameNo"]['ball_3'] or '' }}</div>
    @endif
  </div>
  <div class="row">
    <div class="col-xs-12 total">{{ $game["frame_$frameNo"]['score'] or '' }}</div>
  </div>
</div>