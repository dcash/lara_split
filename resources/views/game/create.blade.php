@extends('layouts.header')
@section('content')
<div class="row">
  <div class="col-xs-4 col-xs-offset-4 text-center">
    <div id="saveWait"></div>
    <div id="saveSuccess" class="hidden"><span aria-hidden="true" class="glyphicon glyphicon-ok-sign"></span></div>
    <div id="saveFail" class="hidden"><span aria-hidden="true" class="glyphicon glyphicon-remove-sign"></span></div>
  </div>
  <div class="col-xs-4 text-right">
    <div id="saveGame" onclick="saveGame()" class="btn btn-info mobile disabled">save</div><a href="{{ url('games/create') }}" id="newGame" class="btn btn-success smobile hidden">new game</a>
  </div>
</div>

@include('game.scoreboard')

<hr/>

<div class="col-xs-6">
  <div role="presentation" class="active"><a href="#input-keys" aria-controls="Pinfall" role="tab" data-toggle="tab">total pinfall</a></div>
</div>
<div class="col-xs-6 text-right">
  <div role="presentation"><a href="#input-pins" aria-controls="Pins" role="tab" data-toggle="tab">pin by pin</a></div>
</div>

<div class="row">
  <div class="col-xs-10 col-xs-offset-1">
    <div id="score10" value="10" class="btn btn-success mobile score-btn btn-fill">strike</div>
  </div>
</div>
<div class="tab-content">
  <div id="input-keys" role="tabpanel" class="tab-pane active">
    <div class="row">
      <div class="col-xs-2 col-xs-offset-1">
        <div id="score0" value="0" class="btn btn-success mobile score-btn">0</div>
      </div>
      <div class="col-xs-2">
        <div id="score1" value="1" class="btn btn-success mobile score-btn">1</div>
      </div>
      <div class="col-xs-2">
        <div id="score2" value="2" class="btn btn-success mobile score-btn">2</div>
      </div>
      <div class="col-xs-2">
        <div id="score3" value="3" class="btn btn-success mobile score-btn">3</div>
      </div>
      <div class="col-xs-2">
        <div id="score4" value="4" class="btn btn-success mobile score-btn">4</div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-2 col-xs-offset-1">
        <div id="score5" value="5" class="btn btn-success mobile score-btn">5</div>
      </div>
      <div class="col-xs-2">
        <div id="score6" value="6" class="btn btn-success mobile score-btn">6</div>
      </div>
      <div class="col-xs-2">
        <div id="score7" value="7" class="btn btn-success mobile score-btn">7</div>
      </div>
      <div class="col-xs-2">
        <div id="score8" value="8" class="btn btn-success mobile score-btn">8</div>
      </div>
      <div class="col-xs-2">
        <div id="score9" value="9" class="btn btn-success mobile score-btn">9</div>
      </div>
    </div>
  </div>
  <div id="input-pins" role="tabpanel" class="tab-pane">
    <div class="row row-centered text-center">
      <div class="col-xs-2 col-centered">
        <div id="pin-7" class="pin">7</div>
      </div>
      <div class="col-xs-2 col-centered">
        <div id="pin-8" class="pin">8</div>
      </div>
      <div class="col-xs-2 col-centered">
        <div id="pin-9" class="pin">9</div>
      </div>
      <div class="col-xs-2 col-centered">
        <div id="pin-10" class="pin">10</div>
      </div>
    </div>
    <div class="row row-centered text-center">
      <div class="col-xs-2 col-centered">
        <div id="pin-4" class="pin">4</div>
      </div>
      <div class="col-xs-2 col-centered">
        <div id="pin-5" class="pin">5</div>
      </div>
      <div class="col-xs-2 col-centered">
        <div id="pin-6" class="pin">6</div>
      </div>
    </div>
    <div class="row row-centered text-center">
      <div class="col-xs-2 col-centered">
        <div id="pin-2" class="pin">2</div>
      </div>
      <div class="col-xs-2 col-centered">
        <div id="pin-3" class="pin">3</div>
      </div>
    </div>
    <div class="row row-centered text-center">
      <div class="col-xs-2 col-centered">
        <div id="pin-1" class="pin">1</div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-8 col-xs-offset-2">
        <div id="doneBtn" class="btn btn-success mobile btn-fill" onclick="recordPins()">done</div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-8 col-xs-offset-2">
      <div id="scoreCLR" onclick="removeScore()" class="btn btn-danger mobile score-btn-clr btn-fill disabled">backspace</div>
    </div>
  </div>
</div>
<script type="text/javascript" src="{{ asset('js/add.js') }}"></script>
@endsection