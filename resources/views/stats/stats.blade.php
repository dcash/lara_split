
@extends('layouts.header')
@section('content')
<div class="row">

  <div class="col-xs-12 large">Stats from: 
    @if ((isset($day)))
      @if ((isset($number)))
        <a href="{{ url('stats/day/'.explode(' ', $day)[0]) }}">{{ @formattedDate($day) }}</a>
        <br/>
        {{ 'Game '.$number }}
      @else
        {{ @formattedDate($day) }}
      @endif
    @else
      All Time
    @endif
  </div>
</div>
  @if (isset($day))
    <div class="row">
      <div class="col-xs-12">
        <a href="{{ url('/stats') }}">
          <div class="btn btn-default mobile">View All</div>
        </a>
      </div>
    </div>
  @endif
<hr/>
<div class="row">
  <div class="col-xs-6 text-right large">Average :</div>
  <div class="col-xs-6 large">{{ $stats['average'] }}</div>
</div>
<hr/>
<div class="row">
  <div class="col-xs-8">
    <h1 class="large">Highlights</h1>
  </div>
</div>
<hr/>
<div class="row">
  <div class="col-xs-6 text-right">Games:</div>
  <div class="col-xs-4">{{ $stats['games'] }}</div>
</div>
<div class="row">
  <div class="col-xs-6 text-right">Balls Thrown:</div>
  <div class="col-xs-4">{{ $stats['balls'] }}</div>
</div>
<div class="row">
  <div class="col-xs-6 text-right">Strikes:</div>
  <div class="col-xs-4">{{ $stats['strikes'] }}</div>
</div>
<div class="row">
  <div class="col-xs-6 text-right">Spares:</div>
  <div class="col-xs-4">{{ $stats['spares'] }}</div>
</div>
<div class="row">
  <div class="col-xs-6 text-right">High Score:</div>
  <div class="col-xs-4">{{ $stats['highScore'] }}</div>
</div>
<div class="row">
  <div class="col-xs-6 text-right">Most Strikes:</div>
  <div class="col-xs-4">{{ $stats['mostStrikes'] }}</div>
</div>
<div class="row">
  <div class="col-xs-6 text-right">Longest Streak:</div>
  <div class="col-xs-4">{{ $stats['streak'] }}</div>
</div>
<hr/>
<div class="row">
  <div class="col-xs-12">
    <h1 class="large">First Ball Breakdown</h1>
  </div>
</div>
<hr/>
<div class="row">
  <div id="firstBallPie" style="height:500px;" class="col-xs-12 chart"></div>
</div>
<br/>
@if (count($stats['ballOnePercents']) > 0)
  <div class="row">
    <div class="col-xs-12 text-center">Pinfall</div>
  </div>
  @foreach ($stats['ballOnePercents'] as $pin=>$percent)
    <div class="row">
      <div class="col-xs-6 text-right">Pin {{ $pin }}:</div>
      <div class="col-xs-4">{{ $percent }}%</div>
    </div>
  @endforeach
@endif
<hr/>
<div class="row">
  <div class="col-xs-12">
    <h1 class="large">Spare Breakdown</h1>
  </div>
</div>
<hr/>
<div class="row">
  <div class="col-xs-12 text-center">Number of Attempts: {{ $stats['secondBalls'] }}</div>
</div>
<div class="row">
  <div id="secondBallPie" style="height:500px;" class="col-xs-12 chart"></div>
</div>
<div class="row">
  <div class="col-xs-12">
    <?php $keys = array_keys($stats['spareStats']);
          sort($keys);
          $i = 0;
          $statLen = count($keys); ?>

      @while($i < $statLen)
        <?php $spare = $keys[$i]; ?>
        <div class="row">
          <div class="col-xs-6">
            <div class="row">
              @if ($spare == 1)
                <div class="col-xs-12"><?= $spare ?> Pin Left:</div>
              @else
                <div class="col-xs-12"><?= $spare ?> Pins Left:</div>
              @endif
              <div class="row">
                <div id="{{ $spare.'sparePie'}}" style="height:250px;" class="col-xs-12 chart"></div>
              </div>
            </div>
          </div>
          @if(isset($keys[$i+1]))
            <?php $spare2 = $keys[$i+1]; ?>
            <div class="col-xs-6">
              <div class="row text-right">
                @if ($spare2 == 1)
                  <div class="col-xs-12">{{ $spare2 }} Pin Left:</div>
                @else
                  <div class="col-xs-12">{{ $spare2 }} Pins Left:</div>
                @endif
                <div class="row">
                  <div id="{{ $spare2.'sparePie'}}" style="height:250px;" class="col-xs-12 chart"></div>
                </div>
              </div>
            </div>
          @endif
        </div>
        <?php $i += 2; ?>
      @endwhile
  </div>
</div>
<br/>
@if (count($stats['ballTwoPercents']) > 0)
  <div class="row">
    <div class="col-xs-12 text-center">Single Pin Percents</div>
  </div>
  @foreach ($stats['ballTwoPercents'] as $pin=>$percent)
    <div class="row">
      <div class="col-xs-6 text-right">Pin {{ $pin }}:</div>
      <div class="col-xs-4">{{ $percent }}%</div>
    </div>
  @endforeach
@endif
<hr/>
<div class="row">
  <div class="col-xs-8">
    <h1 class="large">Per Game Stats:</h1>
  </div>
</div>
<hr/>
<div class="row">
  <div id="gamePie" style="height:500px;" class="col-xs-12 chart"></div>
</div>
<hr/>
<div class="row">
  <div class="col-xs-8">
    <h1 class="large">Per Frame Stats:</h1>
  </div>
</div>
<hr/>
<div class="row">
  <div class="col-xs-12">
    <div class="col-xs-1 frame-cnt-stack text-center">
      <div class="row">
        <div class="col-xs-12 frame-cnt-head">Fr</div>
      </div>
      <div class="row">
        <div class="col-xs-12">X</div>
      </div>
      <div class="row">
        <div class="col-xs-12">/</div>
      </div>
      <div class="row">
        <div class="col-xs-12">O</div>
      </div>
      <div class="row">
        <div class="col-xs-12 frame-cnt-body-xs">AVG</div>
      </div>
    </div>
      @foreach($stats['frameStats'] as $frame=>$fstats)
        <div class="col-xs-1 frame-cnt-stack text-center">
          <div class="row">
            <div class="col-xs-12 frame-cnt-head">{{ $frame }}</div>
          </div>
          <div class="row">
            <div class="col-xs-12 frame-cnt-body">{{ $fstats['strikes'] }}</div>
          </div>
          <div class="row">
            <div class="col-xs-12 frame-cnt-body">{{ $fstats['spares'] }}</div>
          </div>
          <div class="row">
            <div class="col-xs-12 frame-cnt-body">{{ $fstats['opens'] }}</div>
          </div>
          <div class="row">
            <div class="col-xs-12 frame-cnt-body-sm">{{ $fstats['avg'] }}</div>
          </div>
        </div>
      @endforeach
  </div>
</div>
<script type="text/javascript" src="{{ asset('js/stats.js') }}"></script>
<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}"></script>
<script type='text/javascript'>
  var stats = {!! json_encode($stats) !!}
  var gameStats = {Strikes:stats.strikesGame,Spares:stats.sparesGame,Opens:stats.opensGame};
  drawFirstBallPie(stats.avgFirst);
  drawSecondBallPie(stats.avgSpare);
  drawGamePie(gameStats);
  drawSparePies(stats.spareStats);
</script>
@endsection
