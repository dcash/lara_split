@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                
                <div class="panel-body">
                    @foreach ($games as $game)
                        <p>{{ $game->created_at }}</p>
                        <p>
                        @for ($i=1; $i<11; $i++)
                            <?php $frameNum = "frame_$i"; ?>

                            Frame {{ $i }}: {{ $game->frame($game->{$frameNum})->score }}
                        @endfor
                        </p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
